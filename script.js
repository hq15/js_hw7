/*
DOM (Document Object Model) - это содержимое страницы, представленная в виде многомерного объекта, в котором все теги а также текст внутри тегов являются объектами. Благодаря объектной модели нам доступен каждый элемент на странице с возможностью их изменения.
*/

const list = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parentElem = document.querySelector("body");

addingList(list, parentElem);

function addingList(arr, parent) {
  parent.insertAdjacentHTML("beforeend", `<ul>${arr.map(item => `<li>${item}</li>`).join("")}</ul>`);
}
